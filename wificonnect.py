import network
import ujson

def connect():

    file = open("config.txt", "r")
    config = ujson.loads(file.read())
    file.close()

    print("config.txt:")
    print(config)
    print(config["ssid"])
    print(config["password"])

    ssid = config["ssid"]
    password = config["password"]
    station = network.WLAN(network.STA_IF)

    if station.isconnected() == True:
        print("Already connected")
        print(station.ifconfig())
        return

    station.active(True)
    station.connect(ssid, password)

    while station.isconnected() == False:
        pass

    print("Connection successful")
    print(station.ifconfig())



